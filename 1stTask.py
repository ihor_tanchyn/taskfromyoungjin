# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 18:01:21 2016

@author: IgorTanchyn
"""

from lazyreading import read_line_by_line
from re import compile

# use generator to read lines from file
file_generator = read_line_by_line('logfile')

found_data = []
count_404_responses = 0

# compile pattern to find line with special IP and 404 response
pattern_ip = compile('75\.98\.230\.254.+" (\d{3})')

for line in file_generator:
    if pattern_ip.search(line):
        response = pattern_ip.search(line).group(1)
        found_data.append(line) 
        if response == '404':
            count_404_responses += 1
    
print 'Total num of 404 responses for 75.98.230.254: {}'.format(count_404_responses)

# output into file
with open('1_Results.txt', 'w') as result:
    result.write('Total num of 404 responses for 75.98.230.254:  {}\n'.format(count_404_responses))
    result.write('\nAll access from 75.98.230.254:\n')
    for elem in found_data:
        result.write('  {}'.format(elem))
