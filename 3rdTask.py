# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 18:34:53 2016

@author: IgorTanchyn
"""

from lazyreading import read_line_by_line
from re import compile

file_generators = read_line_by_line('logfile')

# compile pattern to find IP address in line with GET request and 404 response
pattern_ip = compile('(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}).+GET.+" 404 \d+') 
result_dict = {}   

for line in file_generators:
    if pattern_ip.search(line):
        ip = pattern_ip.search(line).group(1)
        # count num of ip`s occurrences
        result_dict[ip] = result_dict.get(ip, 0) + 1

# output results in file
with open('3_Results.txt', 'w') as result:
    result.write('IP address and the count of 404 apache responses for GET requests: \n\n')
    result.write('{} | Num of 404\n{}\n'.format('IP Address'.center(16, " "), 30*'-'))
    for key, value in result_dict.items():    
        result.write(' {:15s} : {}\n'.format(key, str(value).center(10, ' ')))
