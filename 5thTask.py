# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 20:14:16 2016

@author: IgorTanchyn
"""

from lazyreading import read_line_by_line
from re import compile
from operator import itemgetter

file_generators = read_line_by_line('logfile')

# find IP in line with GET request and 404 response
pattern_ip = compile('(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}).+"GET\s.+" 404 \d+.+') 

result_dict = {}

for line in file_generators:
    if pattern_ip.search(line):
        ip = pattern_ip.search(line).group(1)
        result_dict[ip] = result_dict.get(ip, 0) + 1

# sort result_dict by key in descending order and print first 10 elements
print "Top 10 blocked IP: \n"
for it, value in enumerate(sorted(result_dict.items(), key = itemgetter(1), reverse = True)):
    if it == 10: break
    print ' {:15s} : {}'.format(value[0], value[1])