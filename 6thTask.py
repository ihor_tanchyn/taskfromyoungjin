# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 20:26:02 2016

@author: IgorTanchyn
"""

from lazyreading import read_line_by_line
from re import compile
from operator import itemgetter

file_generator = read_line_by_line('logfile')

# find IP and URL in line with GET request and 404 response
pattern_get = compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b.+"GET\s(.*)\sHTTP.+" 404 \d+.+')

result_dict = {}

for line in file_generator:
    if pattern_get.search(line):
        ip, url = pattern_get.search(line).groups()
        if url in result_dict:
            result_dict[url][0] += 1
            if ip not in result_dict[url][1]:
                result_dict[url][1].append(ip)
        else:
            result_dict[url] = [1,[ip]]

# output result data in file
with open('6_Results.txt', 'w') as result:
    result.write('')
    # sort dictionary in descending order by key
    for it, value in enumerate(sorted(result_dict.items(), key = itemgetter(1), reverse = True)):
        if it == 10: break    
        result.write('{}. {} : Count of 404 responses: {}\nIP addresses: {}\n'.format(it + 1, value[0], value[1][0], value[1][1]))