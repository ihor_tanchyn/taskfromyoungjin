# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 18:25:14 2016

@author: IgorTanchyn
"""

from lazyreading import read_line_by_line
from re import compile

file_generator = read_line_by_line('logfile')

pattern_ip = compile('75\.98\.230\.254.+"\s(\d{3})')    
result_dict = {}

for line in file_generator:
    if pattern_ip.search(line):
        response = pattern_ip.search(line).group(1)
        # add response as key to dictionary, to count num of its occurrences
        result_dict[response] = result_dict.get(response, 0) + 1

# print results in the console
print "Response | Counts\n{}".format(16*'-')
for key, value in result_dict.items():
    print '{:8s} : {}'.format(key, value)