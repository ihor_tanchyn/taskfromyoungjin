# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 17:59:04 2016

@author: IgorTanchyn
"""

def read_line_by_line(file_name):
    with open(file_name, 'r') as logfile:
        while True:
            line = logfile.readline()
            if not line:
                break
            yield line