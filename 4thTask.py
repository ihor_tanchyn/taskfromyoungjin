# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 19:25:30 2016

@author: IgorTanchyn
"""

from lazyreading import read_line_by_line
from re import compile

file_generator = read_line_by_line('logfile')

# find IP address in line with GET request, PHP file in URL and 404 response
pattern_ip_php = compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b.+"GET\s.*\.php.*\sHTTP.+" 404 \d+.+')
result_dict = {}
count_404_responses = 0

for line in file_generator:
    if pattern_ip_php.search(line):
        ip = pattern_ip_php.search(line).group(1)
        count_404_responses += 1 # count num of 404 responses
        result_dict[ip] = result_dict.get(ip, 0) + 1

# output result data in file
print 'Total num of 404 responses: {}'.format(count_404_responses)
with open('4_Results.txt', 'w') as result:
    result.write('IP address and the count of 404 apache responses for GET requests of files with file extension .php:\n\n')
    result.write('{} | Num of 404\n{}\n'.format('IP Address'.center(16, " "), 30*'-'))
    for key, value in result_dict.items():    
        result.write(' {:15s} : {}\n'.format(key, str(value).center(10, ' ')))

